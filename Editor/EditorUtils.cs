using System;
using UnityEditor;

namespace MLFramework.Editor
{
    public static class EditorUtils
    {
        public static object AdaptiveField(string label, object value)
        {
            if (value.GetType().IsEnum)
            {
                return EditorGUILayout.EnumPopup(label, (Enum) value);
            }

            return value;
        }

        public static object AdaptiveField(string label, float value)
        {
            return EditorGUILayout.FloatField(label, value);
        }

        public static object AdaptiveField(string label, int value)
        {
            return EditorGUILayout.IntField(label, value);
        }

        public static object AdaptiveField(string label, string value)
        {
            return EditorGUILayout.TextField(label, value);
        }
    }
}