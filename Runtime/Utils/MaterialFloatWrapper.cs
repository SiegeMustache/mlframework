﻿using System;
using UnityEngine;

namespace MLFramework.Utility
{
    public class MaterialFloatWrapper : MonoBehaviour
    {
        public Renderer TargetRenderer;
        public int TargetMaterial;
        public string TargetParameter;

        public float Value;
        
        private void Update()
        {
                if (TargetRenderer.materials[TargetMaterial].GetFloat(TargetParameter) != Value)
                    TargetRenderer.materials[TargetMaterial].SetFloat(TargetParameter, Value);
        }
    }
}