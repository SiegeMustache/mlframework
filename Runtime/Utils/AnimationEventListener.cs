﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Utility
{
    [Serializable]
    public struct AnimationEventInfo
    {
        public string TargetEvent;
        public UnityEvent EventTriggered;
    }

    public class AnimationEventListener : MonoBehaviour
    {
        [SerializeField] private AnimationEventInfo[] m_Events;

        public void OnAnimationEvent(string eventName)
        {
            foreach (var animationEventInfo in m_Events)
            {
                if (eventName == animationEventInfo.TargetEvent)
                    animationEventInfo.EventTriggered?.Invoke();
            }
        }
    }
}