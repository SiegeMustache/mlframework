﻿using System;
using System.Collections.Generic;

namespace MLFramework.Data {
public class DataContext {
    private Dictionary<string, object> m_Datas;

    public DataContext() {
        m_Datas = new Dictionary<string, object>();
    }

    public void SetData<T>(string key, T value) {
        if (m_Datas.ContainsKey(key)) {
            if (m_Datas[key].GetType() != typeof(T)) {
                throw new Exception("Context Data Type Mismatch");
            }

            m_Datas[key] = value;
            return;
        }
        
        m_Datas.Add(key, value);
    }

    public T GetData<T>(string key) {
        if (m_Datas.ContainsKey(key) && m_Datas[key].GetType() == typeof(T)) {
            return (T) m_Datas[key];
        }
        
        throw new Exception("No value found");
    }

    public bool Contains(string key)
    {
        if (m_Datas.ContainsKey(key) && m_Datas[key] != null)
            return true;
        return false;
    }

    public DataContext Clone()
    {
        var result = new DataContext();
        foreach (var data in m_Datas)
        {
            result.SetData(data.Key, data.Value);
        }

        return result;
    }
}
}