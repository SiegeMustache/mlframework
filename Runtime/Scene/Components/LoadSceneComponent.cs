﻿using UnityEngine;

namespace MLFramework.Scene.Components {
public class LoadSceneComponent : MonoBehaviour {
    [SerializeField] private string m_ScenePath;

    public void LoadScene() {
        SceneManager.LoadScene(m_ScenePath);
    }
}
}