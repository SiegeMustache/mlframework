﻿using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Scene.Components {
public class SceneUnloadListenComponent : MonoBehaviour{
    [SerializeField] private string m_ScenePath;
    [SerializeField] private UnityEvent m_SceneUnloaded;

    private void OnEnable() {
        EventsManager.StartListening(EventID.SceneUnloaded, OnSceneUnloaded);
    }

    private void OnDisable() {
        EventsManager.StopListening(EventID.SceneUnloaded, OnSceneUnloaded);
    }

    private void OnSceneUnloaded(GenericEventArgs args) {
        var targetScene = (string) args.Args[0];
        if(targetScene == m_ScenePath)
            m_SceneUnloaded?.Invoke();
    }
}
}