﻿using UnityEngine;

namespace MLFramework.Scene.Components {
public class UnloadSceneComponent : MonoBehaviour {
    [SerializeField] private string m_ScenePath;

    public void UnloadScene() {
        SceneManager.UnloadScene(m_ScenePath);
    }
}
}