﻿using System;
using System.Collections.Generic;
using MLFramework.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MLFramework.Scene {
public class SceneManager : SingletonMonoBehaviour<SceneManager>
{
    [SerializeField] private string[] m_ScenesToLoad;

    public List<string> ActiveScenes { get; private set; }
    
    private static Dictionary<string, Action> m_LoadedCallbacks;
    private static Dictionary<string, Action> m_UnloadedCallbacks;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void Init() {
        Instance.EnableDontDestroy();
        Instance.ActiveScenes = new List<string>();
        Instance.ActiveScenes.Add(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
        UnityEngine.SceneManagement.SceneManager.sceneUnloaded += OnSceneUnloaded;
        m_LoadedCallbacks = new Dictionary<string, Action>();
        m_UnloadedCallbacks = new Dictionary<string, Action>();

        if (Instance.m_ScenesToLoad != null)
        {
            foreach (var scene in Instance.m_ScenesToLoad)
            {
                LoadScene(scene);
            }
        }
    }

    public static void LoadScene(string sceneName, Action sceneLoaded = null) {
        if(sceneLoaded!=null)
            m_LoadedCallbacks.Add(sceneName, sceneLoaded);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }

    private static void OnSceneLoaded(UnityEngine.SceneManagement.Scene scn, LoadSceneMode loadMode) {
        if (m_LoadedCallbacks.ContainsKey(scn.name))
        {
            m_LoadedCallbacks[scn.name]?.Invoke();
            m_LoadedCallbacks.Remove(scn.name);
        }

        Instance.ActiveScenes.Add(scn.name);
        EventsManager.TriggerEvent(EventID.SceneLoaded, null, scn);
    }

    public static void UnloadScene(string sceneName, Action sceneUnloaded = null)
    {
        if (Instance.ActiveScenes.Contains(sceneName))
        {
            if(sceneUnloaded != null)
                m_UnloadedCallbacks.Add(sceneName, sceneUnloaded);
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(sceneName);
        }
    }
    
    private static void OnSceneUnloaded(UnityEngine.SceneManagement.Scene scn) {
        if (m_UnloadedCallbacks.ContainsKey(scn.name))
        {
            m_UnloadedCallbacks[scn.name]?.Invoke();
            m_UnloadedCallbacks.Remove(scn.name);
        }
        Instance.ActiveScenes.Remove(scn.name);
        EventsManager.TriggerEvent(EventID.SceneUnloaded, null, scn);
    }

    public static void SetActiveScene(string targetScene)
    {
        UnityEngine.SceneManagement.SceneManager.SetActiveScene(UnityEngine.SceneManagement.SceneManager.GetSceneByName(targetScene));
    }
}
}