﻿using System.Collections.Generic;
using MLFramework.Pooling;
using UnityEngine;

namespace MLFramework.Audio {
public abstract class SFXEmitterController : PoolableObject {
    [SerializeField] protected float m_LifeTime;
    [SerializeField] protected bool m_Loop = false;
    
    protected float m_Timer;
    protected bool m_Running;

    protected void Update() {
        if (m_Running) {
            m_Timer += Time.deltaTime;
            if (m_Loop)
                Play();
            else
                Stop();
        }
    }

    public virtual void Play(params KeyValuePair<string, float>[] playParameters) {
        m_Running = true;
        m_Timer = 0f;
    }

    public virtual void Stop() {
        m_Running = false;
    }

    public bool IsPlaying() {
        return m_Running;
    }

    private void OnDisable() {
        m_Running = false;
    }
}
}