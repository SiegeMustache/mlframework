﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.Audio.Components {
public class SFXPlayComponent : MonoBehaviour {
    [Serializable]
    public struct SFXPlayInfo {
        [Prefab] public string SFXPrefab;
        public Vector3 Position;
        public Transform Parent;

        public SFXPlayInfo(string sfxPrefab, Vector3 position, Transform parent = null) {
            if(PrefabManager.GetPrefabReference(sfxPrefab) == null)
                throw new Exception("No Prefab Found");

            SFXPrefab = sfxPrefab;
            Parent = parent;
            Position = position;
        }
    } 

    public class VFXSpawnComponent : MonoBehaviour {
        [SerializeField] private List<SFXPlayInfo> m_SFXToSpawn = new List<SFXPlayInfo>();

        public List<SFXPlayInfo> SFXToSpawn => m_SFXToSpawn;

        public void Play() {
            foreach (var sfxPlayInfo in SFXToSpawn) {
                if(sfxPlayInfo.Parent == null)
                    AudioManager.Play(sfxPlayInfo.SFXPrefab, sfxPlayInfo.Position);
                else
                    AudioManager.Play(sfxPlayInfo.SFXPrefab,sfxPlayInfo.Parent);
            }
        }
    }
}
}