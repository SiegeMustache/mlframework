﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.Audio.Components {
public class SFXUnbindComponent : MonoBehaviour {
    [Serializable]
    public struct SFXUnbindInfo {
        [Prefab] public string SFXToUnbind;
        public Transform TargetTransform;
        public bool UnbindAll;

        public SFXUnbindInfo(string sfxToUnbind, Transform targetTransform, bool unbindAll) {
            if(PrefabManager.GetPrefabReference(sfxToUnbind) == null)
                throw new Exception("No Prefab Found");
        
            SFXToUnbind = sfxToUnbind;
            TargetTransform = targetTransform;
            UnbindAll = unbindAll;
        }
    }
    public class VFXUnbindComponent : MonoBehaviour {
        [SerializeField] private List<SFXUnbindInfo> m_SFXToUnbind = new List<SFXUnbindInfo>();

        public List<SFXUnbindInfo> SFXToUnbind => m_SFXToUnbind;

        public void Unbind() {
            foreach (var sfxUnbindInfo in SFXToUnbind) {
                AudioManager.UnbindEmitter(sfxUnbindInfo.SFXToUnbind, sfxUnbindInfo.TargetTransform, sfxUnbindInfo.UnbindAll);
            }
        }
    }
}
}