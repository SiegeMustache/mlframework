﻿using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.Audio.Components {
public class SFXDestroyComponent : MonoBehaviour {
    [SerializeField] public List<SFXEmitterController> m_SFXToDestroy = new List<SFXEmitterController>();

    public List<SFXEmitterController> SFXToDestroy => m_SFXToDestroy;

    public void Destroy() {
        foreach (var sfxEmitterController in SFXToDestroy) {
            sfxEmitterController.Stop();
        }
    }
}
}