﻿using UnityEngine;

namespace MLFramework.VFX {
public class AnimationVFXController : VFXController {
    [SerializeField] private Animator[] m_Animators;

    public Animator[] Animators {
        get {
            if (m_Animators == null)
                m_Animators = GetComponentsInChildren<Animator>();
            return m_Animators;
        }
    }
    
    public override void Play() {
        foreach (var animator in Animators) {
            animator.SetTrigger("Play");
        }
    }

    public override void Stop() {
        foreach (var animator in Animators) {
            animator.SetTrigger("Stop");
        }
    }
}
}