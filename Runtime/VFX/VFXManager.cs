﻿using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.VFX {
public class VFXManager : SingletonMonoBehaviour<VFXManager> {
    private static List<VFXController> m_SpawnedVFX;
    private static Dictionary<Transform, List<VFXController>> m_BoundVFX;
    
    public static List<VFXController> SpawnedVFX => m_SpawnedVFX ?? (m_SpawnedVFX = new List<VFXController>());
    public static Dictionary<Transform, List<VFXController>> BoundVFX => m_BoundVFX ?? (m_BoundVFX = new Dictionary<Transform, List<VFXController>>());

    [RuntimeInitializeOnLoadMethod]
    private static void Init() {
        Instance.EnableDontDestroy();
    }
    
    private void Update() {
        if (SpawnedVFX.Count > 0) {
            Queue<VFXController> vfxToUnload = new Queue<VFXController>();
            foreach (var vfx in SpawnedVFX) {
                if (!vfx.IsPlaying()) {
                    vfxToUnload.Enqueue(vfx);
                }
            }

            while (vfxToUnload.Count > 0) {
                var vfx = vfxToUnload.Dequeue();
                SpawnedVFX.Remove(vfx);
                PrefabManager.DestroyPrefab(vfx);
            }
        }
    }
    
    public static void SpawnVFX(string prefab, Vector3 position) {
        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance) {
            vfxInstance.transform.position = position;
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Play();
            SpawnedVFX.Add(vfxController);
        }
    }
    
    public static void SpawnVFX(string prefab, Vector3 position, Quaternion rotation) {
        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance) {
            vfxInstance.transform.position = position;
            vfxInstance.transform.rotation = rotation;
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Play();
            SpawnedVFX.Add(vfxController);
        }
    }
    
    public static void SpawnVFX(string prefab, Transform parent) {
        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance)
        {
            vfxInstance.transform.SetParent(parent, false);
            vfxInstance.transform.localPosition = Vector3.zero;
            vfxInstance.transform.localRotation = Quaternion.identity;
            vfxInstance.transform.localScale = Vector3.one;
            
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Play();
            SpawnedVFX.Add(vfxController);
        }
    }
    
    public static void SpawnVFX(string prefab, Transform parent, Quaternion globalTargetRotation) {
        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance) {
            vfxInstance.transform.parent = parent;
            vfxInstance.transform.localPosition = Vector3.zero;
            vfxInstance.transform.rotation = globalTargetRotation;
            vfxInstance.transform.localScale = Vector3.one;
            
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Play();
            SpawnedVFX.Add(vfxController);
        }
    }
    
    public static void BindVFX(string prefab, Transform transform) {
        if (PrefabManager.GetPrefabReference(prefab) == null || transform == null) return;

        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance) {
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Stop();

            vfxInstance.transform.parent = transform;

            vfxInstance.transform.localPosition = Vector3.zero;
            vfxInstance.transform.localEulerAngles = Vector3.zero;
            vfxInstance.transform.localScale = Vector3.one;
            
            if (!BoundVFX.ContainsKey(transform))
                BoundVFX.Add(transform, new List<VFXController>());

            BoundVFX[transform].Add(vfxController);
            vfxController.Play();
        }
    }
    
    public static void BindVFX(string prefab, Transform transform, Vector3 offset) {
        if (PrefabManager.GetPrefabReference(prefab) == null || transform == null) return;

        var vfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (vfxInstance) {
            var vfxController = vfxInstance.GetComponent<VFXController>();
            vfxController.Stop();

            vfxInstance.transform.parent = transform;

            vfxInstance.transform.localPosition = offset;
            vfxInstance.transform.localEulerAngles = Vector3.zero;
            vfxInstance.transform.localScale = Vector3.one;
            
            if (!BoundVFX.ContainsKey(transform))
                BoundVFX.Add(transform, new List<VFXController>());

            BoundVFX[transform].Add(vfxController);
            vfxController.Play();
        }
    }
    
    public static void UnbindVFX(string prefab, Transform transform, bool unbindAllInstances = false) {
        if (PrefabManager.GetPrefabReference(prefab) == null || transform == null || !BoundVFX.ContainsKey(transform)) return;

        Queue<VFXController> vfxToUnbind = new Queue<VFXController>();

        foreach (var vfxInstance in BoundVFX[transform]) {
            if (vfxInstance.Prefab == prefab) {
                vfxToUnbind.Enqueue(vfxInstance);
                if (!unbindAllInstances)
                    break;
            }
        }

        while (vfxToUnbind.Count > 0) {
            var vfxInstance = vfxToUnbind.Dequeue();
            BoundVFX[transform].Remove(vfxInstance);
            PrefabManager.DestroyPrefab(vfxInstance);
        }
    }
    
    public static void UnbindVFXAll(Transform transform) {
        if (transform == null || !BoundVFX.ContainsKey(transform)) return;

        Queue<VFXController> vfxToUnbind = new Queue<VFXController>();

        foreach (var vfxInstance in BoundVFX[transform]) {
            vfxToUnbind.Enqueue(vfxInstance);
        }

        while (vfxToUnbind.Count > 0) {
            var vfxInstance = vfxToUnbind.Dequeue();
            BoundVFX[transform].Remove(vfxInstance);
            PrefabManager.DestroyPrefab(vfxInstance);
        }
    }
}
}