﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.VFX.Components {
[Serializable]
public struct VFXUnbindInfo {
    [Prefab] public string VFXToUnbind;
    public Transform TargetTransform;
    public bool UnbindAll;

    public VFXUnbindInfo(string vfxToUnbind, Transform targetTransform, bool unbindAll) {
        if(PrefabManager.GetPrefabReference(vfxToUnbind) == null)
            throw new Exception("No Prefab Found");
        
        VFXToUnbind = vfxToUnbind;
        TargetTransform = targetTransform;
        UnbindAll = unbindAll;
    }
}
public class VFXUnbindComponent : MonoBehaviour {
    [SerializeField] private List<VFXUnbindInfo> m_VFXToUnbind = new List<VFXUnbindInfo>();

    public List<VFXUnbindInfo> VFXToUnbind => m_VFXToUnbind;

    public void Unbind() {
        foreach (var vfxUnbindInfo in VFXToUnbind) {
            VFXManager.UnbindVFX(vfxUnbindInfo.VFXToUnbind, vfxUnbindInfo.TargetTransform, vfxUnbindInfo.UnbindAll);
        }
    }
}
}