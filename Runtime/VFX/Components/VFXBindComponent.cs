﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.VFX.Components {
[Serializable]
public struct VFXBindInfo {
    [Prefab] public string VFXPrefab;
    public Transform Parent;
    public Vector3 Offset;
    
    public VFXBindInfo(string vfxPrefab, Transform parent, Vector3 offset) {
        if(PrefabManager.GetPrefabReference(vfxPrefab) == null)
            throw  new Exception("No Prefab Found");
        
        VFXPrefab = vfxPrefab;
        Parent = parent;
        Offset = offset;
    }
}

public class VFXBindComponent : MonoBehaviour {
    [SerializeField] private List<VFXBindInfo> m_VFXToBind = new List<VFXBindInfo>();

    public List<VFXBindInfo> VFXToBind => m_VFXToBind;

    public void Bind() {
        foreach (var vfxBindInfo in VFXToBind) {
            VFXManager.BindVFX(vfxBindInfo.VFXPrefab, vfxBindInfo.Parent, vfxBindInfo.Offset);
        }
    }
}
}