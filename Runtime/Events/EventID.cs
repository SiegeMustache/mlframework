﻿#if DEFAULT_EVENTS
namespace MLFramework.Events {
public enum EventID {
#region Framework Events
    //Scene
    SceneLoaded,
    SceneUnloaded,
    //Game Flow
    GameStateEnter,
    GameStateExit
#endregion

#region Specific Events

#endregion
}
}
#endif