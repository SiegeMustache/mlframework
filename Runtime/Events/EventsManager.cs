﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace MLFramework.Events {
[Obsolete]
public class GenericEvent : UnityEvent<GenericEventArgs> { };

public static class EventsManager {
    private static Dictionary<EventID, GenericEvent> m_Events;

    public static Dictionary<EventID, GenericEvent> Events => m_Events ?? (m_Events = new Dictionary<EventID, GenericEvent>());

    public static void StartListening(EventID id, UnityAction<GenericEventArgs> listener) {
        GenericEvent thisEvent = null;
        
        if (!Events.TryGetValue(id, out thisEvent)) {
            thisEvent = new GenericEvent();
            Events.Add(id, thisEvent);
        }

        thisEvent.AddListener(listener);
    }

    public static void StopListening(EventID id, UnityAction<GenericEventArgs> listener) {
        GenericEvent thisEvent = null;
        
        if (Events.TryGetValue(id, out thisEvent)) {
            thisEvent.RemoveListener(listener);
        }
    }
    
    public static void TriggerEvent(EventID id) {
        GenericEvent thisEvent = null;
        
        if (Events.TryGetValue(id, out thisEvent))
            thisEvent.Invoke(null);
    }
    
    public static void TriggerEvent(EventID id, object source) {
        GenericEvent thisEvent = null;
        
        if (Events.TryGetValue(id, out thisEvent))
            thisEvent.Invoke(new GenericEventArgs(source));
    }
    
    public static void TriggerEvent(EventID id, object source, params object[] args) {
        GenericEvent thisEvent = null;
        
        if (Events.TryGetValue(id, out thisEvent))
            thisEvent.Invoke(new GenericEventArgs(source, args));
    }
}
}