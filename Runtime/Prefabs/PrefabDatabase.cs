﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.Prefabs {
public class PrefabDatabase : ScriptableObject {
    #region Singleton Pattern

    private static PrefabDatabase m_Instance;

    public static PrefabDatabase Instance {
        get {
            if (m_Instance == null) {
                m_Instance = Resources.Load<PrefabDatabase>("PrefabDatabase");

                // If is called by editor and there's no PrefabDatabase asset file, then create it
#if UNITY_EDITOR
                if (m_Instance == null) {
                    if (!System.IO.Directory.Exists(Application.dataPath + "/_Project"))
                        System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project");

                    if (!System.IO.Directory.Exists(Application.dataPath + "/_Project/Resources"))
                        System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project/Resources");
                    
                    UnityEditor.AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PrefabDatabase>(), "Assets/_Project/Resources/PrefabDatabase.asset");
                    m_Instance = Resources.Load<PrefabDatabase>("PrefabDatabase");
                }
#endif
            }

            return m_Instance;
        }
    }

    #endregion
    
    [SerializeField] private PrefabKeyPair[] m_PrefabList; //Serialized array of references to prefabs

    private static PrefabKeyPair[] PrefabList {
        get { return Instance.m_PrefabList ?? (Instance.m_PrefabList = new PrefabKeyPair[0]); }
        set { Instance.m_PrefabList = value; }
    }

    private static int GetPrefabID(string key) {
        for (int i = 0; i < PrefabList.Length; i++) {
            if (PrefabList[i].Key == key)
                return i;
        }

        return -1;
    }

    public static bool ContainsKey(string key) {
        foreach (var keyPair in PrefabList) {
            if (keyPair.Key == key)
                return true;
        }

        return false;
    }

    public static List<string> GetKeyList(string prefix = "") {
        List<string> ret = new List<string>();
        for (int i = 0; i < PrefabList.Length; i++) {
            if (string.IsNullOrEmpty(prefix) || PrefabList[i].Key.Substring(0, prefix.Length) == prefix)
                ret.Add(PrefabList[i].Key);
        }

        return ret;
    }

    public static List<string> GetKeyListSearched(string search = "") {
        List<string> ret = new List<string>();
        for (int i = 0; i < PrefabList.Length; i++) {
            if (string.IsNullOrEmpty(search) || PrefabList[i].Key.ToUpper().Contains(search.ToUpper()))
                ret.Add(PrefabList[i].Key);
        }

        return ret;
    }

    public static void AddKey(string key) {
        if (ContainsKey(key)) return;

        Array.Resize(ref Instance.m_PrefabList, PrefabList.Length + 1);
        PrefabList[PrefabList.Length - 1] = new PrefabKeyPair(key);

        PrefabList = Utils.Sort(PrefabList);
    }

    public static void DeleteKey(string key) {
        int i = 0;
        PrefabKeyPair[] result = new PrefabKeyPair[PrefabList.Length - 1];

        foreach (var keyPair in PrefabList) {
            if (keyPair.Key == key) continue;
            result[i] = keyPair;
            i++;
        }

        PrefabList = result;
    }

    public static void SetPrefab(string key, GameObject prefab) {
        if (ContainsKey(key)) {
            PrefabList[GetPrefabID(key)].Prefab = prefab;
        }
    }

    public static GameObject GetPrefab(string key) {
        if (!ContainsKey(key))
            return null;

        foreach (var keyPair in PrefabList) {
            if (keyPair.Key == key)
                return keyPair.Prefab;
        }

        return null;
    }

    public List<string> GetPrefixes() {
        var ret = new List<string>();
        foreach (var keyPair in PrefabList) {
            var pref = keyPair.Key.Split('_')[0];
            if (!ret.Contains(pref)) {
                ret.Add(pref);
            }
        }

        return ret;
    }
}
}