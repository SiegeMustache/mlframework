﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.Prefabs.Components {
[Serializable]
public struct SpawnInfo {
    [Prefab] public string PrefabToSpawn;
    public Vector3 SpawnPosition;
    public Vector3 SpawnRotation;
    public Vector3 SpawnScale;
    public Transform Parent;

    public SpawnInfo(string prefabToSpawn, Vector3 spawnPosition, Vector3 spawnRotation, Vector3 spawnScale, Transform parent = null) {
        
        if(PrefabManager.GetPrefabReference(prefabToSpawn) == null)
            throw new Exception("No Prefab Found");
        
        PrefabToSpawn = prefabToSpawn;
        SpawnPosition = spawnPosition;
        SpawnRotation = spawnRotation;
        SpawnScale = spawnScale;
        Parent = parent;
    }
}
public class PrefabSpawnComponent : MonoBehaviour {
    [SerializeField] private List<SpawnInfo> m_PrefabsToSpawn = new List<SpawnInfo>();

    public List<SpawnInfo> PrefabsToSpawn => m_PrefabsToSpawn;

    public void Spawn() {
        foreach (var spawnInfo in PrefabsToSpawn) {
            var spawnedObject = PrefabManager.InstantiatePrefab(spawnInfo.PrefabToSpawn);
            spawnedObject.transform.parent = spawnInfo.Parent;
            spawnedObject.transform.position = spawnInfo.SpawnPosition;
            spawnedObject.transform.rotation = Quaternion.Euler(spawnInfo.SpawnRotation);
            spawnedObject.transform.localScale = spawnInfo.SpawnScale;
        }
    }
}
}