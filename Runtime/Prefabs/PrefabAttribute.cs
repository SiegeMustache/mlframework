﻿using UnityEngine;

namespace MLFramework.Prefabs {
/// <summary>
/// Attribute used to show a string field as a dropdown to select a prefab key inside unity inspector
/// </summary>
public class PrefabAttribute : PropertyAttribute {
    /// <summary>
    /// Prefix to use for filter the prefabs inside the inspector
    /// </summary>
    public readonly string PrefixFilter;

    public PrefabAttribute() {
        PrefixFilter = "";
    }
    
    public PrefabAttribute(string prefixFilter) {
        PrefixFilter = prefixFilter;
    }
}
}