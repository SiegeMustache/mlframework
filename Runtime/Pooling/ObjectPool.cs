﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace MLFramework.Pooling {
/// <summary>
/// Class which define an instance of an object pool.
/// The Object Pool pre-load an amount of specific GameObject to avoid the use of instantiate functions during the gameplay.
/// </summary>
public class ObjectPool {
    #region Private Fields
    private PoolProfile m_Profile;
    private List<GameObject> m_PooledObjects;
    private Queue<GameObject> m_InactiveObjects;
    private Transform m_ParentTransform;
    #endregion

    #region Properties

    /// <summary>
    /// The Profile which define the pool features
    /// </summary>
    public PoolProfile Profile {
        get { return m_Profile; }
        private set { m_Profile = value; }
    }

    /// <summary>
    /// List witch contains all GameObjects of the pool
    /// </summary>
    public List<GameObject> PooledObjects {
        get { return m_PooledObjects ?? (m_PooledObjects = new List<GameObject>()); }
        private set { m_PooledObjects = value; }
    }

    /// <summary>
    /// Queue of activable objects of the pool
    /// </summary>
    public Queue<GameObject> InactiveObjects {
        get { return m_InactiveObjects ?? (m_InactiveObjects = new Queue<GameObject>()); }
        private set { m_InactiveObjects = value; }
    }

    /// <summary>
    /// Parent transform for inactive pool objects
    /// </summary>
    public Transform ParentTransform {
        get { return m_ParentTransform; }
        private set { m_ParentTransform = value; }
    }

    #endregion
    
    #region Constructor

    /// <summary>
    /// Create a new ObjectPool with a specific profile
    /// </summary>
    /// <param name="profile">Profile that describe the pool features</param>
    /// <param name="parentTransform">The parent transform for inactive objects</param>
    /// <exception cref="System.Exception">No profile defined</exception>
    public ObjectPool(PoolProfile profile, Transform parentTransform) {
        if (profile == null)
            throw new Exception("Error, undefined profile");

        Profile = profile;
        ParentTransform = parentTransform;
        LoadProfile(false);
    }
    #endregion
    
     #region Public Methods
    /// <summary>
    /// Instantiate or re-generated the pool with a specific profile
    /// </summary>
    /// <param name="force">Allow destroy active object of existing pool</param>
    /// <exception cref="Exception">Impossible destroy objects of the pool</exception>
    public void LoadProfile(bool force) {
        if (PooledObjects.Count > 0) {
            if (force) {
                foreach (var activeObject in PooledObjects) {
                    Object.Destroy(activeObject);
                }

                PooledObjects = new List<GameObject>();
            }
            else {
                throw new Exception("Error, can't destroy active pool object");
            }
        }

        while (InactiveObjects.Count > 0) {
            Object.Destroy(InactiveObjects.Dequeue());
        }

        for (int i = 0; i < Profile.AmountToLoad; i++) {
            var sourceObject = PrefabDatabase.GetPrefab(Profile.Prefab);
            try
            {
                GameObject go = GameObject.Instantiate(sourceObject, ParentTransform);
                go.SetActive(false);
                InactiveObjects.Enqueue(go);

                foreach(var poolable in go.GetComponents<PoolableObject>())
                    poolable.SetSourcePrefab(Profile.Prefab);
            
                PooledObjects.Add(go);
            }catch(Exception ex)
            {
                Debug.LogError(ex);
            }

            
        }
    }

    /// <summary>
    /// Activate an object from the pool and set it to a specific position with a specific rotation
    /// </summary>
    /// <param name="position">Position where to place the activated GameObject</param>
    /// <param name="rotation">Rotation to apply at the activated GameObject</param>
    /// <returns>The activated GameObject</returns>
    /// <exception cref="Exception">No inactive objects in the pool</exception>
    public GameObject Pull(Vector3 position, Quaternion rotation) {
        GameObject output = null;
        if (InactiveObjects.Count > 0) {
            output = InactiveObjects.Dequeue();
        }
        else {
            bool founded = false;
            for (int i = 0; i < PooledObjects.Count; i++) {
                if (!PooledObjects[i].activeInHierarchy) {
                    if (founded) {
                        InactiveObjects.Enqueue(PooledObjects[i]);
                    }
                    else {
                        output = PooledObjects[i];
                        founded = true;
                    }
                }
            }

            if (!founded && Profile.CanExpand) {
                output = GameObject.Instantiate(PrefabDatabase.GetPrefab(Profile.Prefab), position, rotation);
                PooledObjects.Add(output);
            }
        }

        if (output == null) throw new Exception("Error, can't provide any pooled object");

        output.SetActive(true);
        output.transform.position = position;
        output.transform.rotation = rotation;
        PooledObjects.Add(output);
        output.transform.SetParent(null);
        SceneManager.MoveGameObjectToScene(output, SceneManager.GetActiveScene());
        return output;
    }

    /// <summary>
    /// Activate an object from the pool and set it to a position zero with identity rotation
    /// </summary>
    /// <returns>The activated GameObject</returns>
    /// <exception cref="Exception">No inactive objects in the pool</exception>
    public GameObject Pull() {
        return Pull(Vector3.zero, Quaternion.identity);
    }

    /// <summary>
    /// Insert a GameObject inside the pool
    /// </summary>
    /// <param name="go">GameObject to insert</param>
    public void Push(GameObject go) {
        if (!PooledObjects.Contains(go)) return;
        go.SetActive(false);
        go.transform.SetParent(ParentTransform);
        InactiveObjects.Enqueue(go);
    }
    #endregion
}
}