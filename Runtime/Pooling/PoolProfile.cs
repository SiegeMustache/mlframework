﻿using System;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.Pooling{
[CreateAssetMenu(fileName = "POOL_NewPoolProfile", menuName = "Pooling/Pool Profile")]
public class PoolProfile : ScriptableObject, IComparable {
#region Inspector Fields

    [SerializeField] [Prefab] private string m_Prefab;
    [SerializeField] private int m_AmountToLoad;
    [SerializeField] private bool m_CanExpand;

#endregion

#region Properties

    public string Prefab {
        get { return m_Prefab; }
        set { m_Prefab = value; }
    }

    public int AmountToLoad {
        get { return m_AmountToLoad; }
        set { m_AmountToLoad = value; }
    }

    public bool CanExpand {
        get { return m_CanExpand; }
        set { m_CanExpand = value; }
    }

#endregion

#region Public Methods

    public int CompareTo(object obj) {
        var other = obj as PoolProfile;
        if (other != null) 
            return Prefab == other.Prefab ? 0 : 1;
        throw new InvalidCastException("Error, can't cast" + obj.GetType() + " to " + this.GetType());
    }

#endregion
}
}