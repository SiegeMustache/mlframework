﻿namespace MLFramework.Experimental.Events {
public class GenericEventArgs {
    private object m_Source;
    private object[] m_Args;

    public object Source => m_Source;
    public object[] Args => m_Args;

    public GenericEventArgs(object source, object[] args = null) {
        m_Source = source;
        m_Args = args;
    }
}
}