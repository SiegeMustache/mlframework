﻿using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Experimental.Events.Components {
public class EventTriggerComponent : MonoBehaviour {
    [SerializeField] private EventID m_EventToTrigger;
    [SerializeField] private UnityEvent m_TriggerDone;

    public void Trigger() {
        EventManager.TriggerEvent(m_EventToTrigger, this, () => m_TriggerDone?.Invoke());
    }
}
}