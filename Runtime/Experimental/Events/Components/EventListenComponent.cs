﻿using System;
using System.ComponentModel;
using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Experimental.Events.Components {
public class EventListenComponent : MonoBehaviour
{
    [SerializeField, Tooltip("Release the callback after event trigger")] private bool m_AutomaticallyRelease = true;
    [SerializeField] private bool m_TriggerIfParent;
    [SerializeField] private bool m_TriggerIfObject;
    [SerializeField] private EventID m_EventToListen;
    [SerializeField] private UnityEvent m_ListenTarget;

    private Action m_ReleaseEventCallback;
    
    private void OnEnable() {
        EventManager.AddListener(m_EventToListen, OnEventTriggered);
    }

    private void OnDisable() {
        EventManager.RemoveListener(m_EventToListen, OnEventTriggered);
    }

    private void OnEventTriggered(GenericEventArgs args, Action listenerExecutedCallback)
    {
        var source = args.Source as GameObject;
        if (source != null)
        {
            if (m_TriggerIfObject && source.transform == transform)
            {
                m_ReleaseEventCallback += listenerExecutedCallback;
                m_ListenTarget.Invoke();
                if (m_AutomaticallyRelease)
                    Release();
            }
            else if(m_TriggerIfParent && transform.IsChildOf(source.transform))
            {
                m_ReleaseEventCallback += listenerExecutedCallback;
                m_ListenTarget.Invoke();
                if (m_AutomaticallyRelease)
                    Release();
            }
            else
            {
                listenerExecutedCallback?.Invoke();
            }
        }else if (!m_TriggerIfParent && !m_TriggerIfObject)
        {
            m_ReleaseEventCallback += listenerExecutedCallback;
            m_ListenTarget.Invoke();
            if (m_AutomaticallyRelease)
                Release();
        }
        else
        {
            listenerExecutedCallback?.Invoke();
        }
    }

    public void Release()
    {
        m_ReleaseEventCallback?.Invoke();
        m_ReleaseEventCallback = null;
    }
}
}