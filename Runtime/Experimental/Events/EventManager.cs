﻿using System;
using System.Collections.Generic;
using MLFramework.Events;

namespace MLFramework.Experimental.Events
{
public static class EventManager
{
    public delegate void GenericEvent(GenericEventArgs args, Action listenerExecutedCallback);

    private static Dictionary<EventID, List<GenericEvent>> m_Events;
    private static Dictionary<EventID, List<GenericEvent>> Events => m_Events ?? (m_Events = new Dictionary<EventID, List<GenericEvent>>());

    public static void AddListener(EventID id, GenericEvent listener)
    {
        if (!Events.ContainsKey(id))
            Events.Add(id, new List<GenericEvent>());

        Events[id].Add(listener);
    }

    public static void RemoveListener(EventID id, GenericEvent listener)
    {
        if (Events.ContainsKey(id))
        {
            if (Events[id].Contains(listener))
                Events[id].Remove(listener);
        }
    }

    public static void TriggerEvent(EventID id, object source = null, Action eventTriggeredCallback = null, bool unified = true, params object[] args)
    {
        if (Events.ContainsKey(id) && Events[id].Count > 0)
        {
            var injectedArgs = new GenericEventArgs(source, args);

            if (unified)
            {
                var counter = Events[id].Count;
                foreach (var listener in Events[id])
                {
                    listener?.Invoke(injectedArgs, () =>
                        {
                            counter--;
                            if (counter <= 0)
                                eventTriggeredCallback?.Invoke();
                        }
                    );
                }
            }
            else
            {
                ExecuteNextListener(injectedArgs, 0, id, eventTriggeredCallback);
            }
        }
        else
        {
            eventTriggeredCallback?.Invoke();
        }
    }

    private static void ExecuteNextListener(GenericEventArgs args, int listenerId, EventID id, Action eventExecutedCallback)
    {
        Events[id][listenerId]?.Invoke(args, () =>
            {
                listenerId++;
                if(listenerId < Events[id].Count)
                    ExecuteNextListener(args, listenerId, id, eventExecutedCallback);
                else
                    eventExecutedCallback?.Invoke();
            }
        );
    }
}
}