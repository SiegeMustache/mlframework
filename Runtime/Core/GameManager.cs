﻿using MLFramework.Events;
using UnityEngine;

namespace MLFramework.Core {
public static class GameManager {
    public static GameStates ActualState { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Init() {
        ActualState = GameStates.MainMenu;
        EventsManager.TriggerEvent(EventID.GameStateEnter, null, ActualState);
    }

    public static void ChangeState(GameStates targetState) {
        if (ActualState != targetState) {
            EventsManager.TriggerEvent(EventID.GameStateExit, null, ActualState);
            ActualState = targetState;
            EventsManager.TriggerEvent(EventID.GameStateEnter, null, ActualState);
        }
    }
}
}