﻿using System;
using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Core.Components {
public class EnterGameStateComponent : MonoBehaviour {
    [SerializeField] private GameStates m_TargetState;
    [SerializeField] private UnityEvent m_EnterState;

    private void OnEnable() {
        EventsManager.StartListening(EventID.GameStateEnter, OnStateChanged);
    }

    private void OnDisable() {
        EventsManager.StartListening(EventID.GameStateEnter, OnStateChanged);
    }

    private void OnStateChanged(GenericEventArgs args) {
        var state = (GameStates) args.Args[0];

        if (state == m_TargetState) {
            m_EnterState?.Invoke();
        }
    }
}
}